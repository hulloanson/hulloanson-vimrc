set nocompatible
set modeline
set modelines=1
set noswapfile

" Tabs, indentation
set tabstop=2
set shiftwidth=2
set softtabstop=2
set expandtab
set autoindent
set smarttab

" Visual aids
set showmode
set nu
set relativenumber
set visualbell
"set cursorline
set ruler
set laststatus=2
let mapleader = ","
sy on

" Buffers, Windows
set hidden
set autowriteall

" Command bar
set wildmenu
set wildmode=list:longest
set showcmd
set wildignore+=*/node_modules/*,*.so,*.swp,*.zip     " MacOSX/Linux

" Search and Replace
set ignorecase
set smartcase
set hlsearch
set incsearch
set showmatch
set gdefault

" Formatting
set wrap
"set colorcolumn=85 
set formatoptions=rn1

" Auto commands
"au FocusLost * :wa "Um, doesn't work everywhere...
" Set updatetime 
"set updatetime=1000
" Auto save on idle
"autocmd CursorHold,CursorHoldI * update

" Shortcuts
" visual aids
nnoremap <leader><space> :noh<cr>
" Navigation
nnoremap <leader>w <C-w>v<C-w>l
nnoremap <leader>W <C-w>s<C-w>j
nnoremap <leader>Q <C-w>q
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

" GUI settings
set guifont=Inconsolata\ 14
" Vundle
filetype off                  " required 
" set the runtime path to include Vundle and initialize 
set rtp+=~/.vim/bundle/Vundle.vim 
call vundle#begin() 
" alternatively, pass a path where Vundle should install plugins 
" call vundle#begin('~/some/path/here') 
" let Vundle manage Vundle, required 
Plugin 'VundleVim/Vundle.vim'
Plugin 'Chiel92/vim-autoformat'
Plugin 'scrooloose/nerdcommenter'
Plugin 'geoffharcourt/vim-matchit'
Plugin 'ledger/vim-ledger'
Plugin 'othree/eregex.vim'
Plugin 'leafgarland/typescript-vim.git'
Plugin 'ctrlpvim/ctrlp.vim'

call vundle#end()            " required

syntax on
filetype plugin indent on
