if [ -f "$HOME/.vimrc" ]; then
  mv $HOME/.vimrc $HOME/.vimrc.bak
fi
if [ -d "$HOME/.vim_bak" ];then
  rm -rf $HOME/.vim_bak
fi
if [ -d "$HOME/.vim" ]; then
  mv $HOME/.vim $HOME/.vim_bak
fi

rm -rf $HOME/.vimrc $HOME/.vim

ln -s $PWD/.vimrc $HOME/.vimrc
ln -s $PWD/.vim $HOME/.vim

# Init submodules
cd $HOME/.vim
git submodule init
git submodule update
